<?php

/**
 * @file
 * Administration pages provided by Relation Browser module.
 */

/**
 * Menu callback for admin/content/relation. Displays all relations on the site.
 */
function relation_browser_admin_content() {
  // Grab all relations.
  $relations = array();
  $relations = db_select('relation', 'r')
    ->fields('r')
    ->execute()
    ->fetchAllAssoc('rid');
  
  // For the life of me I could not get a join statement to work between the 
  // relation and relation_type tables on relation_type field, so adding data to 
  // the relation object in a separate query.
  foreach ($relations as $relation) {
    $directional = db_select('relation_type', 'rt')
      ->fields('rt', array('directional'))
      ->condition('relation_type', $relation->relation_type)
      ->execute()
      ->fetchField();
    $relation->directional = $directional;
  }
  return theme('relation_browser_admin_content', array('relations' => $relations));
}

/**
 * Generate a table of all relations on this site.
 */
function theme_relation_browser_admin_content($variables) {
  $relations = $variables['relations'];
  
  // Set up header row.
  $header = array(
    t('Title'),
    t('Type'),
    t('Relation'),
    array('data' => t('Operations'), 'colspan' => '2')
  );

  $rows = array();
  foreach ($relations as $relation) {
    // Get the endpoints for this relation id.
    $endpoints = _relation_browser_get_endpoints($relation->rid);
    
    if (!empty($endpoints)) {
      $entities = array();
      foreach ($endpoints as $endpoint) {
        $entity = reset(entity_load($endpoint['endpoints_entity_type'], array($endpoint['endpoints_entity_id'])));
        $title = entity_label($endpoint['endpoints_entity_type'], $entity);
        $path = entity_uri($endpoint['endpoints_entity_type'], $entity);
        // No consistency among entities, $path['path'] for nodes, $path for files.
        // @TODO Add some logic to process how the different entities return a uri.
        // Nodes, comment, taxonomy seem to work as-is below, files and vocabularies do not.
        // see this issue: http://drupal.org/node/1057242
        $entities[] = array('title' => $title, 'path' => $path['path']);
      }
    }
    
    // Build the column for the relation entities.
    $relation_column = array();
    foreach ($entities as $entity) {
      $relation_column[] = l($entity['title'], $entity['path']);
    }
    
    // Build the rows to pass to the table theme function.
    // Directional is implemented, not sure how well it works.
    $rows[] = array(
      l(t('Relation') . ' ' . $relation->rid, 'relation/' . $relation->rid),
      $relation->relation_type,
      implode(($relation->directional) ? " --> " : " <--> ", $relation_column),
      user_access('edit relations') ? l(t('Edit'), 'relation/' . $relation->rid . '/edit') : '',
      user_access('delete relations') ? l(t('Delete'), 'relation/' . $relation->rid . '/delete') : '',
    );
  }

  // Give a message if there are no relations returned.
  if (empty($rows)) {
    $message = t('There are currently no relations on your site.');

    $rows[] = array(
      array('data' => $message, 'colspan' => 5),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Return endpoints for a given relation id.
 */
function _relation_browser_get_endpoints($rid = NULL) {
  if (!isset($rid) || empty($rid)) {
    return '';
  }
  $relations = array();
  $relations = db_select('field_data_endpoints', 'f')
    ->fields('f')
    ->condition('entity_id', $rid)
    ->orderBy('endpoints_r_index', 'ASC')  // We need to get records ordered by r_index for directional.
    ->execute()
    ->fetchAllAssoc('endpoints_r_index', PDO::FETCH_ASSOC);
  
  return $relations;
}